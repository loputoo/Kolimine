--Päringud, mida tuleb PostgreSQL's käivitada enne migratsiooni

--Tabelis order_order veeru 'extra_services' andmetüübi muutmine ARRAY'st VARCHAR'ks.
ALTER TABLE order_order
ADD extra_services2 VARCHAR;

UPDATE order_order
SET extra_services2= extra_services;

--Veergu andmete sisestamine ja loogeliste sulgude eemaldamine
UPDATE order_order
SET extra_services2= SUBSTRING(extra_services2, 2, LENGTH(extra_services2) - 2);

ALTER TABLE order_order
DROP COLUMN extra_services;

ALTER TABLE order_order
RENAME COLUMN extra_services2 TO extra_services;

--Tabelis order_ordernote veergu 'message' andmetüübi muutmine TEXT'st VARCHAR(1000000)'ks.
ALTER TABLE order_ordernote
ADD message2 VARCHAR(1000000);

UPDATE order_ordernote
SET message2= message;

ALTER TABLE order_ordernote
DROP COLUMN message;

ALTER TABLE order_ordernote
RENAME COLUMN message2 TO message;

--Veeru 'number' väärtuste eest esimese tähe eemaldamine, INTEGERIKS teisendamine ning uude veergu salvestaminne.
ALTER TABLE order_order
ADD order_number_int INT;

--Read, mis ei alga 'DEV-JU'ga
UPDATE order_order
SET order_number_int = CAST(RIGHT(number, LENGTH(number) - 1) AS INT)
WHERE number NOT LIKE 'DEV-JU%';

--Read, mis alga 'DEV-JU'ga,
UPDATE order_order
SET order_number_int = CAST(RIGHT(number, LENGTH(number) - 6) AS INT)
WHERE number LIKE 'DEV-JU%';
