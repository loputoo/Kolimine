--Mail_campain_participant insert lause
INSERT INTO [target_db].[dbo].[mail_campaign_participant] (
	first_name,
	last_name,
	email,
	receive_offers,
	created_at,
	user_id,
	manual_upload
	)
SELECT 
	u.first_name,
	u.last_name,
	u.email,
	u.send_email_offers,
	u.created_at,
	u.id,
	0
FROM [target_db].[dbo].[user] AS u;
