--Order tabeli insert laused

-- Enne primaarvõtme sisestamist.
SET IDENTITY_INSERT [target_db].[dbo].[order] ON; 

--Insert lause primaarvõtme sisestamiseks. 
INSERT INTO [target_db].[dbo].[order] (
	id,
	state_id,
	created_at,
	shipping_method_id,
	submit_state_id,
	send_gift_cards,
	store_delivery_type,
	store_pickup_status,
	locked,
	partner_site,
	send_payment_notification,
	omnisend_synced,
	big_bank_signing_type,
	needs_omnisend_sync,
	nav_order_id,
	nav_invoice_id
	)
SELECT 
	oo.order_number_int,
	(SELECT IIF((oo.status = 'Cancelled') OR (oo.status = 'Returned'), 50, 200)) AS state_id,
	oo.date_placed,
	0,--shipping_mehtod_id, algul 0 pürast update lausega muudan õra.
	3, --submit_state_id
	0, --send_gift_cards
	(SELECT IIF(oo.shipping_code = 'pickup', 1, 0)) AS store_delivery_type,	
	2, --store_pickup_status
	0, --locked
	0, --partner_site
	0, --send_payment_notification
	oo.omnisend_synced,
	0, --big_bank_signing_type
	0, --needs_omnisend_sync
	oo.pim_number,
	oo.pim_invoice_number
FROM [POSTGRES].[source_db].[public].[order_order] AS oo

--Pärast primaarvõtme sisestamist.
SET IDENTITY_INSERT [target_db].[dbo].[order] OFF;

--country_is saamine. Tehtud eraldi muutuja, et kood loetavam oleks. T-sql.
DECLARE @LT_ID INT;
SET @LT_ID = (SELECT id
				FROM [target_db].[dbo].[classificator_value]
				WHERE [target_db].[dbo].[classificator_value].classificator_id = (
						SELECT id 
						FROM [target_db].[dbo].[classificator]  
						WHERE name='Country')AND 
							[target_db].[dbo].[classificator_value].name ='Lithuania');

DECLARE @GB_ID INT;
SET @GB_ID = (SELECT id
				FROM [target_db].[dbo].[classificator_value]
				WHERE [target_db].[dbo].[classificator_value].classificator_id = (
						SELECT id 
						FROM [target_db].[dbo].[classificator] 
						WHERE name='Country') AND
							[target_db].[dbo].[classificator_value].name ='Great Britain');

--Veeru väärtused order_billing'st.
UPDATE [target_db].[dbo].[order] 
SET 
	[target_db].[dbo].[order].billing_first_name = oba.first_name, 
	[target_db].[dbo].[order].billing_last_name = oba.last_name,
	[target_db].[dbo].[order].billing_company_name = oba.company_name, 
	[target_db].[dbo].[order].billing_address = (
		SELECT CONCAT(oba.line1, ' ', oba.line2, ' ', oba.line4, ' ', oba.state)), 
	[target_db].[dbo].[order].billing_postal_code = oba.postcode, 
	[target_db].[dbo].[order].billing_city = oba.line4,
	[target_db].[dbo].[order].billing_county = oba.state, 
	[target_db].[dbo].[order].billing_country = oba.country_id, 
	[target_db].[dbo].[order].billing_mobile_phone = oba.phone_number, 
	[target_db].[dbo].[order].billing_company_reg_nr = oba.company_code,
	[target_db].[dbo].[order].billing_country_id =(
		SELECT IIF(oba.country_id='GB', @GB_ID, @LT_ID)), 
	[target_db].[dbo].[order].is_corporate = oba.is_company_account
FROM [target_db].[dbo].[order] AS o
INNER JOIN
	[POSTGRES].[source_db].[public].[order_order] AS oo 
	ON o.id = oo.order_number_int
	INNER JOIN
		[POSTGRES].[source_db].[public].[order_billingaddress] AS oba 
		ON oo.billing_address_id=oba.id;

-- Veerud 'order_shippingaddress'tabelist
UPDATE [target_db].[dbo].[order] 
SET 
	[target_db].[dbo].[order].shipping_first_name = osa.first_name, 
	[target_db].[dbo].[order].shipping_last_name = osa.last_name,
	[target_db].[dbo].[order].shipping_company_name = osa.company_name, 
	[target_db].[dbo].[order].shipping_address = (
		SELECT CONCAT(osa.line1, ' ', osa.line2, ' ', osa.line4, ' ', osa.state)), 
	[target_db].[dbo].[order].shipping_postal_code = osa.postcode,
	[target_db].[dbo].[order].shipping_phone = osa.phone_number, 
	[target_db].[dbo].[order].shipping_country_id = (
		SELECT IIF(osa.country_id='GB', @GB_ID, @LT_ID)),
	[target_db].[dbo].[order].shipping_city = osa.line4,
	[target_db].[dbo].[order].shipping_county = osa.state,
	[target_db].[dbo].[order].store_id = 1, -- osa.pickup_point_code
	[target_db].[dbo].[order].shipping_country = 'LT',
	[target_db].[dbo].[order].shipping_mobile_phone = osa.phone_number
FROM [target_db].[dbo].[order] AS o
INNER JOIN
	[POSTGRES].[source_db].[public].[order_order] AS oo 
	ON o.id = oo.order_number_int
	INNER JOIN
		[POSTGRES].[source_db].[public].[order_shippingaddress] AS osa 
		ON oo.shipping_address_id=osa.id;

--Veeru billing_email väärtused. order_order'st guest_email tabelis või account_user'st. 
UPDATE [target_db].[dbo].[order] 
SET 
	[target_db].[dbo].[order].billing_email =  
		(SELECT IIF(oo.guest_email IS NOT NULL, oo.guest_email, (
			SELECT IIF(au.email IS NOT NULL , au.email, NULL))))
FROM [target_db].[dbo].[order] AS o
INNER JOIN
	[POSTGRES].[source_db].[public].[order_order] AS oo
	ON o.id = oo.order_number_int
	LEFT JOIN
		[POSTGRES].[source_db].[public].[accounts_user] AS au 
		ON au.id = oo.user_id;	

--Veerud 'order_order' tabelist
UPDATE [target_db].[dbo].[order] 
SET 
	[target_db].[dbo].[order].user_id = oo.user_id, 
	[target_db].[dbo].[order].finalized_at = oo.date_placed, 
	[target_db].[dbo].[order].shipping_price = oo.shipping_incl_tax,
	[target_db].[dbo].[order].order_automatically = (
		SELECT IIF(oo.status = 'Manual_review', 0, 1)), 
	[target_db].[dbo].[order].language_id =(
		SELECT id 
		FROM [target_db].[dbo].[language] 
		WHERE [target_db].[dbo].[language].iso_id=UPPER(oo.language))
FROM [target_db].[dbo].[order] AS o
INNER JOIN
	[POSTGRES].[source_db].[public].[order_order] AS oo 
	ON o.id = oo.order_number_int;

--shipping_is_billing
UPDATE [target_db].[dbo].[order] 
SET  
	[target_db].[dbo].[order].shipping_is_billing =(
		SELECT IIF((
			(osa.search_text=oba.search_text) AND
			(osa.first_name=oba.first_name) AND
			(osa.last_name=oba.last_name) AND
			(osa.line1 = oba.line1) AND
			(osa.line2 = oba.line2) AND
			(osa.line3 = oba.line3) AND
			(osa.line4 = oba.line4) AND
			(osa.state = oba.state) AND
			(osa.postcode = oba.postcode) AND
			(osa.phone_number = oba.phone_number) AND
			(osa.country_id = oba.country_id) AND
			(osa.company_code = oba.company_code) AND
			(osa.company_name = oba.company_name) AND
			(osa.is_company_account = oba.is_company_account) AND
			(osa.VAT_number = oba.VAT_number) AND
			(osa.pickup_point_code = oba.pickup_point_code)
		), 1, 0))
FROM [target_db].[dbo].[order] AS o
INNER JOIN
	[POSTGRES].[source_db].[public].[order_order] AS oo 
	ON o.id = oo.order_number_int
	LEFT JOIN 
		[POSTGRES].[source_db].[public].[order_billingaddress] AS oba
		ON oo.billing_address_id = oba.id
		LEFT JOIN
			[POSTGRES].[source_db].[public].[order_shippingaddress] AS osa
			ON oo.shipping_address_id = osa.id;

--order_date.
--Osa on teinud tellimused ära aga pole maksnud. Need on arvatavasti tühistatud.
UPDATE [target_db].[dbo].[order] 
SET 
	[target_db].[dbo].[order].order_date = ope.date_created 
FROM [target_db].[dbo].[order] AS o
INNER JOIN
	[POSTGRES].[source_db].[public].[order_order] AS oo 
	ON o.id = oo.order_number_int
	INNER JOIN
		[POSTGRES].[source_db].[public].[order_paymentevent] AS ope 
		ON oo.id=ope.order_id;

--shipped_to_client_at.
UPDATE [target_db].[dbo].[order] 
SET 
	[target_db].[dbo].[order].shipped_to_client_at = oon.date_created
FROM [target_db].[dbo].[order] AS o
INNER JOIN
	[POSTGRES].[source_db].[public].[order_order] AS oo 
	ON o.id = oo.order_number_int
	INNER JOIN
		[POSTGRES].[source_db].[public].[order_ordernote] AS oon 
		ON oo.id = oon.order_id 
WHERE oon.message LIKE 'Order shipped%' ;

--shipping_method_id (Enumist)
UPDATE [target_db].[dbo].[order] 
SET 
	[target_db].[dbo].[order].shipping_method_id =(
	SELECT IIF(oo.extra_services = 'indoor_delivery', 13,(
		SELECT IIF(oo.shipping_code = 'ziticity', 1,(
			SELECT IIF((oo.shipping_code = 'dpd') OR
					 (oo.shipping_code  = 'venipak') OR
				 	 (oo.shipping_code  = 'courier'), 2,(
						SELECT IIF(oo.shipping_code = 'pickup', 3,(
							SELECT IIF((oo.shipping_code ='omniva') OR
								 (oo.shipping_code = 'dpd_parcel'), 5,0))))))))))
FROM [target_db].[dbo].[order] AS o
INNER JOIN
	[POSTGRES].[source_db].[public].[order_order] AS oo 
	ON o.id = oo.order_number_int;

--Id järjekorranumbri muutmine. 
DECLARE @NewSeed NUMERIC(10)
SELECT @NewSeed = MAX(id) FROM [target_db].[dbo].[order];
PRINT 'New seed is:'
PRINT @NewSeed
DBCC CHECKIDENT([order], RESEED, @NewSeed);
