--order_product tabeli insert laused

--Luuakse uus veerg nimega 'old_product_id'
--et vanadelt toodete url'delt oleks v�imalik teha �mbersuunamist praegustele url'dele

ALTER TABLE [target_db].[dbo].[order_product]
ADD old_product_id INTEGER;

INSERT INTO [target_db].[dbo].[order_product] (
	Order_Id,
	Product_Id,
	Quantity,
	Price,
	currency_id,
	created_at,
	do_not_check_stock,
	line_amount,
	back_order_quantity,
	buy_in_price,
	price_without_vat,
	old_product_id
	)
SELECT 
	oo.order_number_int,
	p.id,   
	ol.quantity,
	ol.unit_price_incl_tax,
	4,
	ol.date_updated,
	1,
	0,
	0,
	ol.unit_cost_price,
	ol.line_price_excl_tax,
	ol.product_id
FROM target_db.dbo.product AS p 
RIGHT JOIN
    [POSTGRES].[source_db].[public].[order_line] AS ol
    ON (p.code)=CAST(ol.upc AS NVARCHAR)
        LEFT JOIN
        [POSTGRES].[source_db].[public].[order_order] AS oo
        ON oo.id=ol.order_id;
