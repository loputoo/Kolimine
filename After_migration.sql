﻿--P�rast andmesiiret k�ivitada PostgreSQL'is
--Abitabelite kustutamine

--Abiveeru kustutamine
ALTER TABLE order_order
DROP COLUMN order_number_int;

--Veeru 'extra_services'tagasi andmet��pi ARRAY'ks muutmine, et vana rakendus t��taks.
ALTER TABLE order_order
ADD extra_services2 VARCHAR(32)[];

--Stringi muutmine ARRAY'ks
UPDATE order_order
SET extra_services2= string_to_array(extra_services, ',');

ALTER TABLE order_order
DROP COLUMN extra_services;

ALTER TABLE order_order 
RENAME COLUMN extra_services2 TO extra_services;

--veeru 'message'tagasi andmet��pi TEXT'ks muutmine,et vana rakendus t��taks.
ALTER TABLE order_ordernote
ADD message2 TEXT;

UPDATE order_ordernote
SET message2= message;

ALTER TABLE order_ordernote
DROP COLUMN message;

ALTER TABLE order_ordernote 
RENAME COLUMN message2 TO message; 
