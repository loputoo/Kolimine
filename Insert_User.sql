--INSERT ja UPDATE laused User tabelisse

-- Enne primaarvõtme sisestamist.
SET IDENTITY_INSERT [target_db].[dbo].[user] ON;

--Primaarvõtme sisestamine.
INSERT INTO [target_db].[dbo].[user]
            (id,
             is_corporate,
             first_name,
             last_name,
             company_name,
             customer_name,
             email)
SELECT au.id,
       au.is_company_account,
       au.first_name,
       au.last_name,
       au.company_name,
       (SELECT IIF(au.is_company_account = 'TRUE', au.company_name, (
				SELECT CONCAT(au.first_name, ' ', au.last_name)))) AS customer_name,
       au.email
FROM   [POSTGRES].[source_db].[public].[accounts_user] AS au;

--Pärast primaarvõtme sisestamist.
SET IDENTITY_INSERT [target_db].[dbo].[user] OFF;

--Veerud account_user tabelist
UPDATE [target_db].[dbo].[user]
SET
	[target_db].[dbo].[user].is_active=au.is_active,
	[target_db].[dbo].[user].send_email_offers=au.allow_email,
	[target_db].[dbo].[user].send_sms_offers=au.allow_text_message,
	[target_db].[dbo].[user].shipping_email=au.email,
	[target_db].[dbo].[user].created_at=au.date_joined,
	[target_db].[dbo].[user].password= au.password,
	[target_db].[dbo].[user].omnisend_contact_id=au.omnisend_contact_id,
	[target_db].[dbo].[user].last_login_at=au.last_login,
	[target_db].[dbo].[user].vat_registration_nr=au.VAT_number,
	[target_db].[dbo].[user].account_confirmed = (
		SELECT IIF(au.is_active='TRUE', 1, 0)),
	[target_db].[dbo].[user].language_id =(
		SELECT id
		FROM [target_db].[dbo].[language]
		WHERE[target_db].[dbo].[language].iso_id=UPPER(au.language))
FROM [target_db].[dbo].[user] AS u
INNER JOIN
	[POSTGRES].[source_db].[public].[accounts_user] AS au
	ON u.id = au.id;

--Veerud, mille väärtuseks pannakse kindel numbriline väärtus.
UPDATE [target_db].[dbo].[user]
SET	[target_db].[dbo].[user].is_secure_sign_in_confirmed = 0,
	[target_db].[dbo].[user].needs_buum_sync = 0,
	[target_db].[dbo].[user].has_new_password = 0,
	[target_db].[dbo].[user].is_in_lhv_list = 0,
	[target_db].[dbo].[user].is_module_tools_visible = 0,
	[target_db].[dbo].[user].is_salesman_desktop_visible = 0,
	[target_db].[dbo].[user].send_credit_limit_offers = 0,
	[target_db].[dbo].[user].use_electronic_invoice = 0,
	[target_db].[dbo].[user].needs_omnisend_sync = 0,
	[target_db].[dbo].[user].send_invoice = 1,
	[target_db].[dbo].[user].last_login_type = 1,
	[target_db].[dbo].[user].source_id = 116
FROM [target_db].[dbo].[user];

-- facebook_id(social_auth_usersocialauth'st)
UPDATE [target_db].[dbo].[user]
SET
    [target_db].[dbo].[user].facebook_id=sau.uid
FROM [target_db].[dbo].[user] AS u
RIGHT JOIN
	[POSTGRES].[source_db].[public].[social_auth_usersocialauth] AS sau
	ON u.id = sau.user_id
WHERE provider='facebook';

-- Google_id(social_auth_usersocialauth'st)
UPDATE [target_db].[dbo].[user]
SET
	[target_db].[dbo].[user].google_id=sau.uid
FROM [target_db].[dbo].[user] AS u
RIGHT JOIN
	[POSTGRES].[source_db].[public].[social_auth_usersocialauth] AS sau
	ON u.id = sau.user_id
WHERE provider='google-oauth2';

--Lauatelefon leidmine.
--ühel kasutajal võib olla mitu aadressi rida, vaja on võtta kõige uuemalt telefoni number.
UPDATE [target_db].[dbo].[user]
SET
	[target_db].[dbo].[user].shipping_phone = landline_nrs.phone
FROM [target_db].[dbo].[user] AS u
RIGHT JOIN
	(SELECT DISTINCT(au.id) AS old_user_id,
		aua.phone_number AS phone
	FROM [POSTGRES].[source_db].[public].[accounts_user] AS au
	LEFT JOIN
		--millisel kuupäeval loodi iga kasutaja kõige uuem aadressi rida
        (SELECT  user_id,
			MAX(date_created) MaxDate
         FROM    [POSTGRES].[source_db].[public].[address_useraddress]
         GROUP BY user_id) MaxDates ON au.id = MaxDates.user_id
		 LEFT JOIN
			--kõige uuem rida
			[POSTGRES].[source_db].[public].[address_useraddress] aua ON
				MaxDates.user_id = aua.user_id AND 
				MaxDates.MaxDate = aua.date_created
			WHERE aua.is_default_for_shipping='false' AND
				(SUBSTRING(aua.phone_number, 1,4)='+370') AND
				(SUBSTRING(aua.phone_number, 5,1) IN ('3', '4', '5'))) as landline_nrs
ON u.id=landline_nrs.old_user_id;

--Mitte lauatelefonid
--Ühel kasutajal võib olla mitu aadressi rida, vaja on võtta kõige uuemalt telefoni number.
UPDATE [target_db].[dbo].[user]
SET
	[target_db].[dbo].[user].shipping_mobile_phone =notlandline_nrs.phone
FROM [target_db].[dbo].[user] AS u
RIGHT JOIN
	(SELECT DISTINCT(au.id) AS old_user_id,
		            aua.phone_number AS phone
	FROM [POSTGRES].[source_db].[public].[accounts_user] au
	LEFT JOIN
		--millisel kuupäeval loodi iga kasutaja kõige uuem aadressi rida
        (SELECT  user_id,
			    MAX(date_created) MaxDate
         FROM    [POSTGRES].[source_db].[public].[address_useraddress]
         GROUP BY user_id) MaxDates ON au.id = MaxDates.user_id
		 LEFT JOIN
			--Kõige uuem rida
			[POSTGRES].[source_db].[public].[address_useraddress] aua ON
				MaxDates.user_id = aua.user_id AND
				MaxDates.MaxDate = aua.date_created
WHERE (SUBSTRING(aua.phone_number, 1,4)<>'+370') OR (
    (SUBSTRING(aua.phone_number, 1,4)='+370') AND
    (SUBSTRING(aua.phone_number, 5,1) NOT IN ('3', '4', '5')))) AS notlandline_nrs
ON u.id=notlandline_nrs.old_user_id;

--Ülejäänud lahtrid address_useraddress tabelist. join_result on vahetabel
UPDATE [target_db].[dbo].[user]
SET
	[target_db].[dbo].[user].address = (SELECT CONCAT(join_result.line1, ' ', join_result.line2)),
	[target_db].[dbo].[user].phone = join_result.phone_number,
	[target_db].[dbo].[user].postal_code = join_result.postcode,
	[target_db].[dbo].[user].county = join_result.state,
	[target_db].[dbo].[user].country = join_result.country_id,
	[target_db].[dbo].[user].city = join_result.line4,
	[target_db].[dbo].[user].shipping_first_name = join_result.first_name,
	[target_db].[dbo].[user].shipping_last_name = join_result.last_name,
	[target_db].[dbo].[user].shipping_country = join_result.country_id,
	[target_db].[dbo].[user].shipping_county = join_result.state,
	[target_db].[dbo].[user].shipping_address = (SELECT CONCAT(join_result.line1, ' ', join_result.line2)),
	[target_db].[dbo].[user].shipping_postal_code = join_result.postcode,
	[target_db].[dbo].[user].shipping_company_name = join_result.company_name,
	[target_db].[dbo].[user].shipping_city = join_result.line4
FROM [target_db].[dbo].[user] AS u
RIGHT JOIN
	(SELECT DISTINCT(au.id),
		aua.date_created,
		aua.line1,
		aua.line2,
		aua.phone_number,
		aua.postcode,
		aua.state, --county,
		aua.line4, --city,
		aua.first_name,
		aua.last_name,
		aua.country_id,
		aua.company_name
	FROM [POSTGRES].[source_db].[public].[accounts_user] AS au
	RIGHT JOIN
		--millisel kuupäeval loodi iga kasutaja kõige uuem aadressi rida
        (SELECT  user_id,
			MAX(date_created) MaxDate
         FROM    [POSTGRES].[source_db].[public].[address_useraddress]
         GROUP BY user_id) AS MaxDates ON au.id = MaxDates.user_id
		 LEFT JOIN
			--kõige uuem rida
			[POSTGRES].[source_db].[public].[address_useraddress] AS aua ON
				MaxDates.user_id = aua.user_id AND
				MaxDates.MaxDate = aua.date_created) AS join_result
ON u.id=join_result.id;

--Neil, kel is_deafault_for_shipping on määratud tuleb info võtta sellelt realt, mitte viimasest sisestusest.
--aadress ja muu info
UPDATE [target_db].[dbo].[user]
SET
	[target_db].[dbo].[user].address = (
	    SELECT CONCAT(join_result.line1, ' ', join_result.line2)),
	[target_db].[dbo].[user].phone = join_result.phone_number,
	[target_db].[dbo].[user].postal_code = join_result.postcode,
	[target_db].[dbo].[user].county = join_result.state,
	[target_db].[dbo].[user].country = join_result.country_id,
	[target_db].[dbo].[user].city = join_result.line4,
	[target_db].[dbo].[user].shipping_first_name = join_result.first_name,
	[target_db].[dbo].[user].shipping_last_name = join_result.last_name,
	[target_db].[dbo].[user].shipping_country = join_result.country_id,
	[target_db].[dbo].[user].shipping_county = join_result.state,
	[target_db].[dbo].[user].shipping_address = (
	    SELECT CONCAT(join_result.line1, ' ', join_result.line2)),
	[target_db].[dbo].[user].shipping_postal_code = join_result.postcode,
	[target_db].[dbo].[user].shipping_company_name = join_result.company_name,
	[target_db].[dbo].[user].shipping_city = join_result.line4
FROM [target_db].[dbo].[user] AS u
RIGHT JOIN
	(SELECT au.id,
		aua.line1,
		aua.line2,
		aua.phone_number,
		aua.postcode,
		aua.state, --county,
		aua.line4, --city,
		aua.first_name,
		aua.last_name,
		aua.country_id,
		aua.company_name
	FROM [POSTGRES].[source_db].[public].[accounts_user] AS au
	LEFT JOIN
		[POSTGRES].[source_db].[public].[address_useraddress] AS aua ON
		au.id = aua.user_id
		WHERE aua.is_default_for_shipping='true') AS join_result
ON u.id=join_result.id;

-- Neil, kel is_deafault_for_shipping on määratud tuleb info võtta sellelt realt, mitte viimasest sisestusest.
--Lauatelefonid
UPDATE [target_db].[dbo].[user]
SET
	[target_db].[dbo].[user].phone = landline_nrs.phone_number
FROM [target_db].[dbo].[user] AS u
RIGHT JOIN
	(SELECT au.id AS accounts_user_id,
		aua.phone_number
	FROM [POSTGRES].[source_db].[public].[accounts_user] AS au
	LEFT JOIN
		[POSTGRES].[source_db].[public].[address_useraddress] AS aua ON
		au.id = aua.user_id
		WHERE aua.is_default_for_shipping='true' AND
		(SUBSTRING(aua.phone_number, 1,4)='+370') AND (SUBSTRING(aua.phone_number, 5,1) IN ('3', '4', '5'))) AS landline_nrs
ON u.id=landline_nrs.accounts_user_id;

-- Neil, kel is_deafault_for_shipping on määratud tuleb info võtta sellelt realt, mitte viimasest sisestusest.
--Mobiiltelefonid
UPDATE [target_db].[dbo].[user]
SET
	[target_db].[dbo].[user].phone = notlandline_nrs.phone_number
FROM [target_db].[dbo].[user] AS u
RIGHT JOIN
	(SELECT au.id,
		aua.phone_number
	FROM [POSTGRES].[source_db].[public].[accounts_user] AS au
	LEFT JOIN
		[POSTGRES].[source_db].[public].[address_useraddress] AS aua ON
		au.id = aua.user_id
		WHERE aua.is_default_for_shipping='true' AND (
		    (SUBSTRING(aua.phone_number, 1,4)<>'+370') OR
		    (SUBSTRING(aua.phone_number, 1,4)='+370') AND
		    (SUBSTRING(aua.phone_number, 5,1) NOT IN ('3', '4', '5')))) AS notlandline_nrs
ON u.id=notlandline_nrs.id;

--country_id lisamine numbrina
--Leedu ja GB country_id saamine. Tehtud eraldi muutuja, et kood loetavam oleks. T-sql.
DECLARE @LT_ID INT;
SET @LT_ID = (SELECT id
					FROM [target_db].[dbo].[classificator_value]
					WHERE
					  [target_db].[dbo].[classificator_value].classificator_id = (
							SELECT id
								FROM [target_db].[dbo].[classificator]
								WHERE name='Country')
									AND [target_db].[dbo].[classificator_value].name ='Lithuania');

DECLARE @GB_ID INT;
SET @GB_ID = (SELECT id
					FROM [target_db].[dbo].[classificator_value]
					WHERE
					  [target_db].[dbo].[classificator_value].classificator_id = (
							SELECT id
								FROM [target_db].[dbo].[classificator]
								WHERE name='Country')
									AND [target_db].[dbo].[classificator_value].name ='Great Britain');


--country_id lisamine numbrina. Join_result on vahetabel.
UPDATE [target_db].[dbo].[user]
SET
	[target_db].[dbo].[user].shipping_country_id = join_result.country_id_int --country_id_number peaks siia sisse minema
FROM [target_db].[dbo].[user] AS u
RIGHT JOIN
	(SELECT DISTINCT(au.id),
		aua.date_created,
		(SELECT IIF(aua.country_id='GB', @GB_ID, @LT_ID)) AS country_id_int
	FROM [POSTGRES].[source_db].[public].[accounts_user] AS au
	RIGHT JOIN
		--millisel kuupäeval loodi iga kasutaja kõige uuem aadressi rida
        (SELECT  user_id,
			MAX(date_created) MaxDate
         FROM    [POSTGRES].[source_db].[public].[address_useraddress]
         GROUP BY user_id) MaxDates
		    ON au.id = MaxDates.user_id
		 LEFT JOIN
			--milline on iga kasutaja kõige uuem aadressi rida. Sellega saab rea kätte
			[POSTGRES].[source_db].[public].[address_useraddress] AS aua
			    ON MaxDates.user_id = aua.user_id AND MaxDates.MaxDate = aua.date_created) AS join_result
ON u.id=join_result.id;

--Id järjekorranumbri muutmine.
DECLARE @NewSeed NUMERIC(10)
SELECT @NewSeed = MAX(id) FROM [target_db].[dbo].[user];
DBCC CHECKIDENT([user], RESEED, @NewSeed);
